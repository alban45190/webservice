package fr.albanw.video.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Video {

    private Integer id;
    private String titre;
    private String description;
    private UUID authorUuid;
}
