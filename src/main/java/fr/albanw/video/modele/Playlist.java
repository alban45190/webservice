package fr.albanw.video.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Playlist {

    private Integer id;
    private String name;
    private UUID authorUuid;
    List<Integer> videosIds;

}
