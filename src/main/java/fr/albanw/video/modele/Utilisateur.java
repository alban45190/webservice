package fr.albanw.video.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Utilisateur {

    private UUID uuid;
    private String nom;
    private String motDePasse;
    private List<Playlist> playlists;
    private List<Video> videos;

}
