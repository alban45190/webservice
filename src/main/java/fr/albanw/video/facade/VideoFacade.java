package fr.albanw.video.facade;

import fr.albanw.video.exception.*;
import fr.albanw.video.modele.Playlist;
import fr.albanw.video.modele.Utilisateur;
import fr.albanw.video.modele.Video;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VideoFacade {

    private int idVideo = 0;
    private int idPlaylist = 0;

    private ArrayList<Utilisateur> utilisateurs;

    public Utilisateur registerUser(String nom, String motDePasse) throws UtilisateurExistant {
        Utilisateur utilisateur = new Utilisateur(UUID.randomUUID(), nom, motDePasse, new ArrayList<>(), new ArrayList<>());
        utilisateurs.add(utilisateur);
        if(utilisateurs.stream().anyMatch(u -> u.getNom().equals(nom))) {
            throw new UtilisateurExistant();
        }
        return utilisateur;
    }

    public int addVideo(String uuid, String name, String description) throws UtilisateurInexistant {
        UUID id = UUID.fromString(uuid);
        Optional<Utilisateur> utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst();
        if(utilisateur.isEmpty()) {
            throw new UtilisateurInexistant();
        }

        Video video = new Video(idVideo, name, description, id);
        idVideo++;

        utilisateur.get().getVideos().add(video);
        return video.getId();
    }

    public Playlist createPlaylist(String uuid, String titre) throws UtilisateurInexistant{
        UUID id = UUID.fromString(uuid);
        Optional<Utilisateur> utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst();
        if(utilisateur.isEmpty()) {
            throw new UtilisateurInexistant();
        }
        Playlist playlist = new Playlist(idPlaylist, titre, id, Collections.emptyList());
        utilisateur.get().getPlaylists().add(playlist);
        idPlaylist++;
        return playlist;
    }

    public Utilisateur getUser(String uuid) throws UtilisateurInexistant {
        UUID id = UUID.fromString(uuid);
        Optional<Utilisateur> utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst();
        if(utilisateur.isEmpty()) {
            throw new UtilisateurInexistant();
        }
        return utilisateur.get();
    }

    public void deletePlaylist(String uuid, int playlistId) throws UtilisateurInexistant, PlaylistInexistant {
        UUID id = UUID.fromString(uuid);
        Optional<Utilisateur> utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst();
        if(utilisateur.isEmpty()) {
            throw new UtilisateurInexistant();
        }
        if(utilisateur.get().getPlaylists().stream().noneMatch(p -> p.getId().equals(playlistId))) {
            throw new PlaylistInexistant();
        }
        utilisateur.get().getPlaylists().removeIf(p -> p.getId().equals(playlistId));
    }

    public void deleteVideoFromPlaylist(String uuid, int playlistId, int videoId) throws UtilisateurInexistant, PlaylistInexistant, VideoInexistant, VideoPasDansPlaylist {
        UUID id = UUID.fromString(uuid);
        Utilisateur utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst().orElseThrow(UtilisateurInexistant::new);
        Video video = utilisateur.getVideos().stream().filter(v -> v.getId() == videoId).findFirst().orElseThrow(VideoInexistant::new);
        Playlist playlist = utilisateur.getPlaylists().stream().filter(p -> p.getId() == playlistId).findFirst().orElseThrow(PlaylistInexistant::new);
        if(!playlist.getVideosIds().contains(video.getId())) {
            throw new VideoPasDansPlaylist();
        }
        playlist.getVideosIds().remove(videoId);
    }

    public void addVideoToPlaylist(String uuid, int playlistId, int videoId) throws UtilisateurInexistant, PlaylistInexistant, VideoInexistant, VideoDejaDansPlaylist {
        UUID id = UUID.fromString(uuid);
        Utilisateur utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst().orElseThrow(UtilisateurInexistant::new);
        Video video = utilisateur.getVideos().stream().filter(v -> v.getId() == videoId).findFirst().orElseThrow(VideoInexistant::new);
        Playlist playlist = utilisateur.getPlaylists().stream().filter(p -> p.getId() == playlistId).findFirst().orElseThrow(PlaylistInexistant::new);
        if(playlist.getVideosIds().contains(video.getId())) {
           throw new VideoDejaDansPlaylist();
        }
        playlist.getVideosIds().add(videoId);
    }

    public List<Playlist> getPlaylists(String uuid) throws UtilisateurInexistant {
        UUID id = UUID.fromString(uuid);
        Utilisateur utilisateur = utilisateurs.stream().filter(u -> u.getUuid().equals(id)).findFirst().orElseThrow(UtilisateurInexistant::new);
        return utilisateur.getPlaylists();
    }

    public List<Video> getVideos() {
        List<Video> videos = new ArrayList<>();
        for(Utilisateur utilisateur : utilisateurs) {
            videos.addAll(utilisateur.getVideos());
        }
        return videos;
    }
}
