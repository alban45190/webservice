package fr.albanw.video.dto;`

import java.util.UUID;

public record PlaylistDto (Integer id, String nom, UUID authorUuid) { }
