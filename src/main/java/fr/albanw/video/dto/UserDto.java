package fr.albanw.video.dto;

import java.io.Serializable;
import java.util.UUID;

public record UserDto(
        String nom,
        String motDePasse,
        UUID uuid
) implements Serializable { }