package fr.albanw.video.dto;

import java.io.Serializable;
import java.util.UUID;

public record VideoDto(
        Integer id,
        String titre,
        String description,
        UUID authorUuid
) implements Serializable {}