package fr.albanw.video.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class UserDtoDetails implements Serializable {

    private UUID uuid;
    private String nom;
    private String motDePasse;
    private List<VideoDto> videos;
    private List<PlaylistDto> playlists;

}
