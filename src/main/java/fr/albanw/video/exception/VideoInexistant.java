package fr.albanw.video.exception;

public class VideoInexistant extends Exception {
    public VideoInexistant() {
        super("Vidéo introuvable");
    }
}
