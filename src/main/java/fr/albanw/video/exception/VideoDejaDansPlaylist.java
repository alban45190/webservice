package fr.albanw.video.exception;

public class VideoDejaDansPlaylist extends Exception{

    public VideoDejaDansPlaylist() {
        super("La vidéo est déjà dans la playlist");
    }

}
