package fr.albanw.video.exception;

public class PlaylistInexistant extends Exception {

    public PlaylistInexistant() {
        super("Playlist inexistante");
    }

}
