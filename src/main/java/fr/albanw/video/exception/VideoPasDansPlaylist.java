package fr.albanw.video.exception;

public class VideoPasDansPlaylist extends Exception{

    public VideoPasDansPlaylist() {
        super("La vidéo n'est pas dans la playlist");
    }

}
