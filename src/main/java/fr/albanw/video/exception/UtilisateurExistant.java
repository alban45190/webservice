package fr.albanw.video.exception;

public class UtilisateurExistant extends Exception {
    public UtilisateurExistant() {
        super("Utilisateur existant");
    }
}
