package fr.albanw.video.exception;

public class UtilisateurInexistant extends Exception {

    public UtilisateurInexistant() {
        super("Utilisateur inexistant");
    }

}
