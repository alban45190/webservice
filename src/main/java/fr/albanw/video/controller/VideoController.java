package fr.albanw.video.controller;

import fr.albanw.video.dto.PlaylistDto;
import fr.albanw.video.dto.UserDto;
import fr.albanw.video.dto.UserDtoDetails;
import fr.albanw.video.dto.VideoDto;
import fr.albanw.video.exception.*;
import fr.albanw.video.facade.VideoFacade;
import fr.albanw.video.modele.Playlist;
import fr.albanw.video.modele.Utilisateur;
import fr.albanw.video.modele.Video;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;

@RequestMapping(value = "/app")
public class VideoController {

    private final VideoFacade videoFacade;
    private final UriComponentsBuilder base;

    public VideoController(VideoFacade videoFacade, UriComponentsBuilder base) {
        this.videoFacade = videoFacade;
        this.base = base;
    }

    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> registerUser(@RequestBody UserDto userDto) {
        try {
            Utilisateur u = videoFacade.registerUser(userDto.nom(), userDto.motDePasse());
            URI location = base.path("/user/{uuid}").buildAndExpand(u.getUuid()).toUri();
            return ResponseEntity.created(location).build();
        } catch (UtilisateurExistant e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @PostMapping(value = "/video", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Object> addVideo(@RequestHeader String uuid, @RequestParam String name, @RequestParam String description) {
        try {
            int id = videoFacade.addVideo(uuid, name, description);
            return ResponseEntity.created(base.path("/video/{id}").buildAndExpand(id).toUri()).build();
        } catch (UtilisateurInexistant e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping(value = "/playlist", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void createPlaylist(@RequestHeader String uuid, String titre) {
        try {
            Playlist playlist = videoFacade.createPlaylist(uuid, titre);
            URI location = base.path("/playlist/{id}").buildAndExpand(playlist.getId()).toUri();
            ResponseEntity.created(location).build();
        } catch (UtilisateurInexistant e) {
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping(value = "/user/{uuid}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<UserDtoDetails> getUser(@PathVariable String uuid) {
        try {
            Utilisateur utilisateur = videoFacade.getUser(uuid);
            List<VideoDto> videoDtos = new ArrayList<>();
            List<PlaylistDto> playlistDtos = new ArrayList<>();

            utilisateur.getVideos().forEach(v -> videoDtos.add(new VideoDto(v.getId(), v.getTitre(), v.getDescription(), v.getAuthorUuid())));
            utilisateur.getPlaylists().forEach(p -> playlistDtos.add(new PlaylistDto(p.getId(), p.getName(), p.getAuthorUuid())));

            UserDtoDetails userDtoDetails = new UserDtoDetails(utilisateur.getUuid(), utilisateur.getNom(), utilisateur.getMotDePasse(), videoDtos, playlistDtos);
            return ResponseEntity.ok(userDtoDetails);
        } catch (UtilisateurInexistant e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping(value = "/playlist/{playlistId}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> deletePlaylist(@RequestHeader String uuid, @PathVariable int playlistId) {
        try {
            videoFacade.deletePlaylist(uuid, playlistId);
            return ResponseEntity.accepted().build();
        } catch (UtilisateurInexistant | PlaylistInexistant e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }
    @DeleteMapping(value = "/playlist/{playlistId}/video/{videoId}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Object> deleteVideoFromPlaylist(@RequestHeader String uuid, @PathVariable String playlistId, @PathVariable String videoId) {
        try {
            videoFacade.deleteVideoFromPlaylist(uuid, Integer.parseInt(playlistId), Integer.parseInt(videoId));
            return ResponseEntity.accepted().build();
        } catch (UtilisateurInexistant e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (PlaylistInexistant | VideoPasDansPlaylist | VideoInexistant e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping(value = "/playlist/{playlistId}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Object> addVideoToPlaylist(@RequestHeader String uuid, @PathVariable int playlistId,  @RequestParam int videoId) {
        try {
            videoFacade.addVideoToPlaylist(uuid, playlistId, videoId);
            return ResponseEntity.accepted().build();
        } catch (UtilisateurInexistant e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (PlaylistInexistant | VideoInexistant e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (VideoDejaDansPlaylist e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @GetMapping(value = "/playlist", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Collection<PlaylistDto>> getPlaylists(@RequestHeader String uuid) {
        try {
            List<Playlist> playlist = videoFacade.getPlaylists(uuid);
            Collection<PlaylistDto> dto = new ArrayList<>(Collections.emptyList());
            playlist.forEach(p -> dto.add(new PlaylistDto(p.getId(), p.getName(), p.getAuthorUuid())));
            return ResponseEntity.ok(dto);
        } catch (UtilisateurInexistant e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping(value = "/videos", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Collection<VideoDto>> getVideos() {
        Collection<Video> videos = videoFacade.getVideos();

        Collection<VideoDto> videoDtos = new java.util.ArrayList<>(Collections.emptyList());
        videos.forEach(v -> videoDtos.add(new VideoDto(v.getId(), v.getTitre(), v.getDescription(), v.getAuthorUuid())));

        return ResponseEntity.ok(videoDtos);
    }





}
